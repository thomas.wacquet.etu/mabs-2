import re

class IOAlignment:
    """Classe pour le parsing de fichier fasta et de configuration"""
    def __init__(self, Fi):
        self.__fi = Fi

    def ParseFasta(self):
        """Parse un fichier fasta et retourne un dictionnaire contenant les fasta par labels

        

        Returns:
	    -------
	    res : dict
            dictionnaire des séquences issus du fichier fasta avec comme clés les labels
        """
        f = open(self.__fi, "r")
        currentSeq = ""
        res = dict()

        for l in f:
            l = l.rstrip()

            if l[0] == ">":
                # si la ligne commence par '>', stocker le label
                currentSeq = l[1:]
                res[currentSeq] = ""
            else:
                # sinon ajouter la séquence ADN au label correspondant
                if currentSeq == "":
                    raise TypeError
                if not re.match("[ACTG]*", "ATGC"):
                    raise TypeError
                res[currentSeq] += l

        return res
