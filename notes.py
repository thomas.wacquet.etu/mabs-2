# Comparer reads (humains mais pas uniquement) avec génome (virus)
# Output final = séparer les reads issus et non issus du génome

# Table des suffixes => génome du virus => série de positions => pas stocker listes des suffixes
# Fragmenté read en k-mer => chercher si k-mer dans génome virus
# seed -> extend 

# Lecture avec Biopython 
#Alignement avec Parasail

from IOalignment import IOAlignment
import gzip
from Bio import SeqIO
from Bio.Seq import Seq
import parasail

sequence_dict = {}

# Lectures des reads :
# with gzip.open('SRR10971381_1.fastq.gz', 'rt') as fastq:
#     for i, record in enumerate(SeqIO.parse(fastq, 'fastq'), start=1):
#         sequence_dict[i] = (str(record.seq))
#         print(sequence_dict)

# Lectures de Grippe A :
# seq = IOAlignment('Grippe_A.fna')
# seq = seq.ParseFasta() # ID long => garder l'entier en entier ?
# print('séq = ', seq)

print('----------------')
# [ i for i in range(len(genome))]
        
# from multiprocessing.pool import ThreadPool
# from time import sleep
# from random import random

# array=[("AACG",1),("AGGA",2),("TGCA",3),("GGAA",4),("ATTT",5),("CCAC",6),("GCAC",7),("TAGG",8)]

# def process(data1,data2):
#     print(data1,data2)
#     sleep(1)

# pool = ThreadPool(processes=2)
# for data in array:
#     pool.apply_async(process,data)
# pool.close()
# pool.join()

#  This function performs a semi-global alignment. The "dx" in the function name stands for "do not penalize gaps at the beginning of the query sequence and at the end of the 
# database sequence" 1. The "sat" part indicates that the function attempts to allocate profiles for both 8-bit and 16-bit solutions, choosing the appropriate one based on the size of the data 1.
# 5 et 1 gap opening/extension
# parasail.dnafull, specifies the substitution matrix to use for scoring the alignment. parasail.dnafull is a predefined substitution matrix for DNA sequences that includes all possible nucleotide substitutions
al = parasail.sg_dx_scan_sat("TGTTGCATCTTCAGCTAGTCTGGAGCAA", "GGACAGGGCTTTGAGTGGATGTGATGGATCATCACCTACACTGGGAACCCAACGTATACC", 5, 1, parasail.dnafull)
print(al.score)

al=parasail.sg_dx_trace_scan_sat("TGTTGCATCTTCAGCTAGTCTGGAGCAA", "GGACAGGGCTTTGAGTGGATGTGATGGATCATCACCTACACTGGGAACCCAACGTATACC", 5,1,parasail.dnafull)
traceback=al.get_traceback()
print(traceback.ref+"||||||||"+traceback.query)

import tqdm
import time
import tqdm
j=0
for i in tqdm.tqdm(range(int(10e8))):
    j=j+i

progress=tqdm(total=100)
for i in range(100):
    time.sleep(.1) #Justepourralentirleprogrammepourvoirl"effetdetqdm
    progress.update(1)
progress.close()

def compare_my_tuples(tuple):
    return tuple[1]

l=[(2.5,"A"),(4.1,"D"),(1.1,"C"),(3.7,"F"),(2.8,"B")]
sorted(l,key=compare_my_tuples)

