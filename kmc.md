Résultats kmc avec k-mer de 27:
Stage 1: 100%
Stage 2: 100%
1st stage: 12.2781s
2nd stage: 24.6005s
Total    : 36.8785s
Tmp size : 1809MB

Stats:
   No. of k-mers below min. threshold :    243017427
   No. of k-mers above max. threshold :            0
   No. of unique k-mers               :    315431169
   No. of unique counted k-mers       :     72413742
   Total no. of k-mers                :   1644690745
   Total no. of reads                 :     28282964
   Total no. of super-k-mers          :    186607305
